### formview 编辑

```
    const action_xml_id = 'contacts.action_contacts'
    const action = await this.api.env.action(action_xml_id)
    const listview = action.listview
    const dataList = await listview.model.pageGoto(1)
    console.log(dataList)
    const rid = dataList[2].id
    const formview = action.formview
    const model = formview.model
    await model.read(rid)
    console.log(model.values, model)
    const name_required = model.get_required('name', model.values)
    const currency_id_readonly = model.get_readonly('currency_id', model.values)
    console.log(name_required, currency_id_readonly)
    await model.onchange('mobile', '1390101')
    console.log(model.values)
    await model.commit()
    console.log(model.values)

```

1. model = action.formview.model 编辑页面的数据, 都是通过 model 对象进行管理的
2. model.read, 与查询一样, 先读取数据
3. model.values 是一个对象, 可以做为 页面表单的 数据来源
4. model.get_required 是一个函数, 检查该字段是否 必须填(不能为空), 可用于页面的表单验证
5. model.get_readonly 是一个函数, 检查该字段是否只读, 不可编辑. 页面表单上应设置为不可编辑
6. model.onchange 是一个异步函数, 调取模型方法 onchange, 返回值为, 因该字段变化而引起的其他字段更新后的值
7. model.values 这里的数据被 onchange 更新, 页面重新渲染
8. model.commit 是异步函数, 调取模型方法 write, 更新数据到服务器. 然后再次调编辑后的数据
9. model.values,这里的数据被 更新. 页面重新渲染

### formview 新增

```
    const action_xml_id = 'contacts.action_contacts'
    const action = await this.api.env.action(action_xml_id)
    const formview = action.formview
    const model = formview.model
    await model.onchange()
    console.log(model.values)
    await model.onchange('name', 'ttt2')
    console.log(model.values)
    await model.onchange('email', 'ttt2')
    console.log(model.values)
    await model.onchange('mobile', '1390101')
    console.log(model.values)
    await model.commit()
    console.log(model.values)


```

1. model = action.formview.model 与编辑页面相同, 通过 model 对象进行数据的管理
2. model.onchange, 调用 onchange 方法时, 参数为空, 则是新增, 访问 模型 方法 default_get
3. model.values 这里的数据被 onchange 更新, 页面重新渲染, 显示 default_get 的返回值
4. model.commit, 新增时, 调取模型方法 create, 更新数据到服务器. 然后读取新增的数据
5. model.values,这里的数据被 更新. 页面重新渲染
